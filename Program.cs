﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace Lab1
{
    class Notebook
    {
        public static List<Note> notebook = new List<Note>();
        static void Main(string[] args)
        {
            Console.WriteLine("Добро пожаловать в записную книжку!");
            bool end = true;
            while (end)
            {
                Console.WriteLine("Введите номер желаемого действия!");
                Console.WriteLine("1. Добавить новую запись");
                Console.WriteLine("2. Редактирование записи");
                Console.WriteLine("3. Удаление записи");
                Console.WriteLine("4. Просмотр записи");
                Console.WriteLine("5. Просмотр всех записей");
                Console.WriteLine("6. Выход из программы");
                int x;
                while (!(Int32.TryParse(Console.ReadLine(),out x) && x>0 && x<7))
                {
                    Console.WriteLine("Ошибка, попробуйте снова ввести номер необходимого действия: ");
                }

                Note item;
                switch (x)
                {
                    case 1:
                        AddNote();
                        break;
                    case 2:
                        if (notebook.Count != 0){
                            item = ChoiceNote();
                            if (item != null) EditNote(item);
                            break;
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Записей не найдено! Для возвращения в меню нажмите \"Enter\"!");
                            Console.ReadLine();
                            break;
                        }
                    case 3:
                        if (notebook.Count != 0)
                        {
                            item = ChoiceNote();
                            if (item != null) DeleteNote(item);
                            break;
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Записей не найдено! Для возвращения в меню нажмите \"Enter\"!");
                            Console.ReadLine();
                            break;
                        }
                    case 4:
                        if (notebook.Count != 0)
                        {
                            item = ChoiceNote();
                            if (item != null) ReadNote(item);
                            break;
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Записей не найдено! Для возвращения в меню нажмите \"Enter\"!");
                            Console.ReadLine();
                            break;
                        }
                    case 5:
                        if (notebook.Count != 0)
                        {
                            ShowAllNotes();
                            break;
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Записей не найдено! Для возвращения в меню нажмите \"Enter\"!");
                            Console.ReadLine();
                            break;
                        }
                    case 6:
                        end = false;
                        break;
                }
                Console.Clear();
            }
        }

        public static void AddNote ()
        {
            string lastName;
            string name;
            string middleName = "";
            long phone;
            string country;
            DateTime birthDate = default(DateTime);
            string corp = "";
            string position = "";
            string notes = "";
            string s;

            Console.Clear();
            Console.WriteLine("Введите фамилию: ");
            s = Console.ReadLine();
            while (s == "")
            {
                Console.WriteLine("Значение не было введено, попробуйте снова: ");
                s = Console.ReadLine();
            }
            lastName = s;
            Console.Clear();
            Console.WriteLine("Введите имя: ");
            s = Console.ReadLine();
            while (s == "")
            {
                Console.WriteLine("Значение не было введено, попробуйте снова: ");
                s = Console.ReadLine();
            }
            name = s;
            Console.Clear();
            Console.WriteLine("Введите отчество (если оно отсутствует нажмите \"Enter\"): ");
            s = Console.ReadLine();
            middleName = s;
            Console.Clear();
            Console.WriteLine("Введите номер телефона: ");
            s = Console.ReadLine();
            while (s == "")
            {
                Console.WriteLine("Значение не было введено, попробуйте снова: ");
                s = Console.ReadLine();
            }
            phone = long.Parse(s);
            Console.Clear();
            Console.WriteLine("Введите страну: ");
            s = Console.ReadLine();
            while (s == "")
            {
                Console.WriteLine("Значение не было введено, попробуйте снова: ");
                s = Console.ReadLine();
            }
            country = s;
            Console.Clear();
            Console.WriteLine("Введите дату рождения (если она отсутствует нажмите \"Enter\"): ");
            s = Console.ReadLine();
            if (s!="") birthDate = DateTime.Parse(s);
            Console.Clear();
            Console.WriteLine("Введите организацию (если она отсутствует нажмите \"Enter\"): ");
            s = Console.ReadLine();
            corp = s;
            Console.Clear();
            Console.WriteLine("Введите должность (если оно отсутствует нажмите \"Enter\"): ");
            s = Console.ReadLine();
            position = s;
            Console.Clear();
            Console.WriteLine("Введите прочие заметки (если они отсутствуют нажмите \"Enter\"): ");
            s = Console.ReadLine();
            notes = s;
            Console.Clear();
            notebook.Add(new Note(lastName, name, middleName, phone, country, birthDate, corp, position , notes));
        }

        public static Note ChoiceNote()
        {
            Console.Clear();
            Console.WriteLine("Выберите запись для выбранного действия!");
            foreach (var item in notebook)
            {
                string s;
                Console.WriteLine($"Запись: {item.LastName} {item.Name} {item.Phone}");
                Console.WriteLine("Если вы хотите продолжать работу с этой записью " +
                                  "введите \"да\", иначе нажмите \"Enter\" или введите что-то другое");
                s = Console.ReadLine();
                if (s == "да")
                {
                    return item;
                }
                Console.Clear();
            }

            return null;
        }

        public static void EditNote(Note note)
        {
            bool end = true;
            Console.Clear();
            while (end)
            {
                Console.WriteLine("Введите номер поля записи, которое хотите изменить или добавить");
                Console.WriteLine("1. Фамилия");
                Console.WriteLine("2. Имя");
                Console.WriteLine("3. Отчество");
                Console.WriteLine("4. Номер телефона");
                Console.WriteLine("5. Страна");
                Console.WriteLine("6. Дата рождения");
                Console.WriteLine("7. Организация");
                Console.WriteLine("8. Должность");
                Console.WriteLine("9. Прочие заметки");
                Console.WriteLine("10. Вернуться в меню");
                int x;
                
                while (!(Int32.TryParse(Console.ReadLine(), out x) && x > 0 && x < 11))
                {
                    Console.WriteLine(
                        "Ошибка, попробуйте снова ввести номер поля, которое хотите изменить или добавить: ");
                }
                string s;
                Console.Clear();
                switch (x)
                {
                    case 1:
                        Console.WriteLine("Введите новое значение поля: ");
                        s = Console.ReadLine();
                        if (s != "")
                        {
                            notebook[notebook.FindIndex(x => x.theSame(note))].LastName = s;
                            note.LastName = s;
                        }
                        break;
                    case 2:
                        Console.WriteLine("Введите новое значение поля: ");
                        s = Console.ReadLine();
                        if (s != "")
                        {
                            notebook[notebook.FindIndex(x => x.theSame(note))].Name = s;
                            note.Name = s;
                        }
                        break;
                    case 3:
                        Console.WriteLine("Введите новое значение поля: ");
                        s = Console.ReadLine();
                        if (s != "")
                        {
                            notebook[notebook.FindIndex(x => x.theSame(note))].MiddleName = s;
                            note.MiddleName = s;
                        }
                        break;
                    case 4:
                        Console.WriteLine("Введите новое значение поля: ");
                        s = Console.ReadLine();
                        if (s != "")
                        {
                            notebook[notebook.FindIndex(x => x.theSame(note))].Phone = long.Parse(s);
                            note.Phone = long.Parse(s);
                        }
                        break;
                    case 5:
                        Console.WriteLine("Введите новое значение поля: ");
                        s = Console.ReadLine();
                        if (s != "")
                        {
                            notebook[notebook.FindIndex(x => x.theSame(note))].Country = s;
                            note.Country = s;
                        }
                        break;
                    case 6:
                        Console.WriteLine("Введите новое значение поля: ");
                        s = Console.ReadLine();
                        if (s != "")
                        {
                            notebook[notebook.FindIndex(x => x.theSame(note))].BirthDate = DateTime.Parse(s);
                            note.BirthDate = DateTime.Parse(s);
                        }
                        break;
                    case 7:
                        Console.WriteLine("Введите новое значение поля: ");
                        s = Console.ReadLine();
                        if (s != "")
                        {
                            notebook[notebook.FindIndex(x => x.theSame(note))].Corp = s;
                            note.Corp = s;
                        }
                        break;
                    case 8:
                        Console.WriteLine("Введите новое значение поля: ");
                        s = Console.ReadLine();
                        if (s != "")
                        {
                            notebook[notebook.FindIndex(x => x.theSame(note))].Position = s;
                            note.Position = s;
                        }
                        break;
                    case 9:
                        Console.WriteLine("Введите новое значение поля: ");
                        s = Console.ReadLine();
                        if (s != "")
                        {
                            notebook[notebook.FindIndex(x => x.theSame(note))].Notes = s;
                            note.Notes = s;
                        }
                        break;
                    case 10:
                        end = false;
                        break;
                }
                Console.Clear();
            }
        }

        public static void DeleteNote(Note note)
        {
            notebook.Remove(note);
        }

        public static void ReadNote(Note note)
        {
            Console.Clear();
            Console.WriteLine("Запись:");
            Console.WriteLine($"Фамилия: {note.LastName}");
            Console.WriteLine($"Имя: {note.Name}");
            if (note.MiddleName !="") Console.WriteLine($"Отчество: {note.MiddleName}");
            Console.WriteLine($"Номер телефона: {note.Phone}");
            Console.WriteLine($"Страна: {note.Country}");
            if (note.BirthDate != default(DateTime))  Console.WriteLine($"Дата рождения: {note.BirthDate:dd.MM.yyyy}");
            if (note.Corp != "") Console.WriteLine($"Организация: {note.Corp}");
            if (note.Position != "") Console.WriteLine($"Должность: {note.Position}");
            if (note.Notes != "") Console.WriteLine($"Прочие заметки: {note.Notes}");
            Console.WriteLine("Нажмите \"Enter\", чтобы вернуться в меню!");
            Console.ReadLine();
        }

        public static void ShowAllNotes()
        {
            Console.Clear();
            foreach (var item in notebook)
            {
                Console.WriteLine(item.LastName + " " + item.Name+ " " + item.Phone);
            }

            Console.WriteLine("Нажмите \"Enter\", чтобы вернуться в меню!");
            Console.ReadLine();
        }
    }

    class Note
    {
        public string LastName { get; set; }
        public string Name { get; set; }
        public string MiddleName { get; set; }
        public long Phone { get; set; }
        public string Country { get; set; }
        public DateTime BirthDate { get; set; }
        public string Corp { get; set; }
        public string Position { get; set; }
        public string Notes { get; set; }

        public Note(string lastName, string name, string middleName, long phone, string country, DateTime birthDate, string corp, string position, string notes)
        {
            LastName = lastName;
            Name = name;
            MiddleName = middleName;
            Phone = phone;
            Country = country;
            BirthDate = birthDate;
            Corp = corp;
            Position = position;
            Notes = notes;
        }

        public bool theSame(Note note)
        {
            if (LastName == note.LastName &&
                Name == note.Name &&
                MiddleName == note.MiddleName &&
                Phone == note.Phone &&
                Country == note.Country &&
                BirthDate == note.BirthDate &&
                Corp == note.Corp &&
                Position == note.Position &&
                Notes == note.Notes)
            {
                return true;
            }

            return false;
        }
    }
}
